using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Engine
{
	// Base
	private double m_baseEngineGain = double.NaN;
	private double m_friction = double.NaN;

	// Turbo
	private bool m_turboActive = false;
	private int m_turboTicksRemaining = 0;
	private double m_turboFactor = 1;

	// Activation (delayed)
	private bool m_turboActivationPending = false;
	private int m_turboTicksAtFullThrottle = 0;

	/// <summary>
	/// Friction
	/// </summary>
	public double Friction { get { return m_friction; } }
	/// <summary>
	/// Current turbo factor
	/// </summary>
	public double TurboFactor { get { return (m_turboActive && m_turboTicksRemaining > 0 ? m_turboFactor : 1); } }
	/// <summary>
	/// Current engine power
	/// </summary>
	public double EnginePower { get { return m_baseEngineGain * TurboFactor; } }
	/// <summary>
	/// Is turbo currently active (declared by server)
	/// </summary>
	public bool TurboActive { get { return m_turboActive; } }
	/// <summary>
	/// Is turbo available
	/// </summary>
	public bool TurboAvailable { get { return (!m_turboActivationPending && !m_turboActive && m_turboTicksRemaining > 0); } }

	public Engine(double engineGain, double friction)
	{
		m_baseEngineGain = engineGain;
		m_friction = friction;
	}

	/// <summary>
	/// Call at the end of "carPositions" to process engine internals
	/// </summary>
	public void OnEndTick(int gameTick, double currentThrottle)
	{
		// Drain turbo
		if (m_turboActive)
		{
			if (m_turboTicksRemaining > 0)
			{
				--m_turboTicksRemaining;
				if (currentThrottle > 0.99)
					++m_turboTicksAtFullThrottle;
			}
		}
	}

	public void OnTurboAvailable(int duration, double factor)
	{
		if (!m_turboActivationPending && !m_turboActive && m_turboTicksRemaining == 0)
		{
			m_turboTicksRemaining = duration;
			m_turboFactor = factor;
		}
		else
		{
			Console.WriteLine("OnTurboAvailable called when a turbo was already active or was inactive but had ticks remaining.");
		}
	}

	public void OnTurboStart()
	{
		m_turboActive = true;
		m_turboActivationPending = false;
		m_turboTicksAtFullThrottle = 0;
	}

	public void OnTurboStop()
	{
		m_turboActive = false;
		Console.WriteLine("Turbo with full throttle for {0} ticks.", m_turboTicksAtFullThrottle);
	}

	public void OnSpawn()
	{
		m_turboActive = false;
		m_turboActivationPending = false;
		m_turboTicksRemaining = 0;
	}

	public SendMsg ActivateTurbo()
	{
		m_turboActivationPending = true;
		Console.WriteLine("Crikey!");
		return new Turbo("Crikey!");
	}



	/// <summary>
	/// Calculates speed reached after one tick at a specific throttle
	/// </summary>
	public double GetSpeedAfterOneTick(double fromSpeed, double throttle)
	{
		return (fromSpeed + EnginePower * throttle - fromSpeed * m_friction);
	}

	/// <summary>
	/// Calculates throttle to try and reach target speed in one tick
	/// </summary>
	public double GetThrottleToAccelerateInOneTick(double fromSpeed, double toSpeed)
	{
		double engineGainNeeded = toSpeed - fromSpeed + fromSpeed * m_friction;
		double throttle = Math.Max(engineGainNeeded / EnginePower, 0.0);
		return Math.Min(throttle, 1.0);
	}

	/// <summary>
	/// Calculates braking distance to ready target speed with zero throttle
	/// </summary>
	public double GetBrakingDistance(double fromSpeed, double toSpeed)
	{
		double distance = 0.0f;

		while (fromSpeed > toSpeed) //TODO: This can overshoot a little. Not dangerous because then we get a slightly higher distance.
		{
			var a = -(fromSpeed * m_friction);
			var speedAfterTick = fromSpeed + a;
			distance += fromSpeed + a / 2;

			fromSpeed = speedAfterTick;
		}

		return distance;
	}

	public Engine GetClone()
	{
		Engine clone = new Engine(m_baseEngineGain, m_friction);
		clone.m_turboActive = m_turboActive;
		clone.m_turboTicksRemaining = m_turboTicksRemaining;
		clone.m_turboFactor = m_turboFactor;
		clone.m_turboActivationPending = m_turboActivationPending;
		clone.m_turboTicksAtFullThrottle = m_turboTicksAtFullThrottle;
		return clone;
	}
}
