using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

class BoxWriter
{
	private string m_format;
	private object[] m_values;

	public int Height { get { return m_values.Length; } }

	public BoxWriter(string format, int height)
	{
		m_format = format;
		m_values = new object[height];
	}

	public void PushNewValue(object value)
	{
		for (int i = 0; i < Height - 1; ++i)
			m_values[i] = m_values[i + 1];
		m_values[Height - 1] = value;
	}

	public void Draw(int left, int top)
	{
		if (Bot.MainBuild)
			return;

		int cursorLeft = Console.CursorLeft;
		int cursorTop = Console.CursorTop;

		int cursorPos = top;
		for (int i = 0; i < Height; ++i)
		{
			if (m_values[i] == null)
				continue;
			Console.SetCursorPosition(left, cursorPos++);
			Console.Write(m_format, m_values[i]);
		}

		Console.SetCursorPosition(cursorLeft, cursorTop);
	}
}
