using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public abstract class AI
{
	protected enum State
	{
		Telemetry_EnginePower,
		Telemetry_Friction,
		Telemetry_Sliding,
		Race
	}

	protected enum RaceStage
	{
		Qualification,
		Race
	}


	/// <summary>
	/// Handles incoming messages.
	/// </summary>
	/// <returns>Reply to server.</returns>
	public SendMsg OnReceiveMessage(IncomingMsgWrapper msg)
	{
		SendMsg reply = null;

		switch (msg.msgType)
		{
			case "carPositions":
				reply = ProcessTick((JArray)msg.data, m_initTickDone == false, msg.gameTick);
				m_initTickDone = true;
				break;
			case "join":
				Console.WriteLine("[{0,5}][join]", msg.gameTick);
				break;
			case "joinRace":
				Console.WriteLine("[{0,5}][joinRace]", msg.gameTick);
				break;
			case "yourCar":
				ProcessYourCar((JObject)msg.data, msg.gameTick);
				break;
			case "gameInit":
				ProcessGameInit((JObject)msg.data, msg.gameTick);
				m_initTickDone = false;
				break;
			case "gameEnd":
				Console.WriteLine("[{0,5}][gameEnd]", msg.gameTick);
				break;
			case "gameStart":
				Console.WriteLine("[{0,5}][gameStart]", msg.gameTick);
				break;
			case "lapFinished":
				ProcessLapFinished((JObject)msg.data, msg.gameTick);
				break;
			case "crash":
				ProcessCrash((JObject)msg.data, msg.gameTick);
				break;
			case "spawn":
				ProcessSpawn((JObject)msg.data, msg.gameTick);
				break;
			case "turboAvailable":
				ProcessTurbo((JObject)msg.data, msg.gameTick);
				break;
			case "turboStart":
				ProcessTurboStart((JObject)msg.data, msg.gameTick);
				break;
			case "turboEnd":
				ProcessTurboEnd((JObject)msg.data, msg.gameTick);
				break;
			default:
				Console.WriteLine("[{0,5}][!]: {1}", msg.gameTick, msg.msgType);
				break;
		}

		// Tech specs
		bool mustReply = (msg.msgType == "gameStart");
		mustReply |= (msg.msgType == "carPositions" && msg.gameTick >= 0);

		if (!mustReply)
			return null;
		return (reply == null) ? new Ping() : reply;
	}

	protected void ProcessYourCar(JObject msg, int gameTick)
	{
		m_carName = (string)msg["name"];
		m_carColor = (string)msg["color"];

		Console.WriteLine("[{0,5}][yourCar] {1} in {2} trunks.", gameTick, m_carName, m_carColor);
	}

	protected void ProcessGameInit(JObject msg, int gameTick)
	{
		JToken anyCarDim = msg["race"]["cars"][0]["dimensions"];
		Suspension.InitializeCar((double)anyCarDim["width"], (double)anyCarDim["length"], (double)anyCarDim["guideFlagPosition"]);

		JToken raceSession = msg["race"]["raceSession"];
		string extraText = "";

		m_raceStage = (raceSession["durationMs"] != null) ? RaceStage.Qualification : RaceStage.Race;
		if (m_raceStage == RaceStage.Qualification)
		{
			m_qualificationDurationMs = (int)raceSession["durationMs"];
			extraText = string.Format("Duration: {0} ms", m_qualificationDurationMs);
		}

		Console.WriteLine("[{0,5}][gameInit] Stage: {1}. {2}", gameTick, m_raceStage, extraText);

		m_track = new Track();
		m_track.Initialize(msg);

		m_crashed = false;
	}

	protected void ProcessLapFinished(JObject msg, int gameTick)
	{
		var lapTime = msg["lapTime"];
		int lap = (int)lapTime["lap"];
		int ticks = (int)lapTime["ticks"];
		int millis = (int)lapTime["millis"];

		string name = (string)msg["car"]["name"];
		if (name == m_carName)
		{
			Console.WriteLine("[{0,5}][lapFinished] Lap: {1}, Ticks: {2}, Millis: {3}", gameTick, lap, ticks, millis);
			m_totalLaps++;
		}
		//else
			//Console.WriteLine("[{0,5}][lapFinished] <Opponent: {1}> Lap: {2}, Ticks: {3}, Millis: {4}", gameTick, name, lap, ticks, millis);
	}

	protected void ProcessCrash(JObject msg, int gameTick)
	{
		Piece piece = m_track.Pieces[m_currentPieceIndex];

		string name = (string)msg["name"];
		if (name == m_carName)
		{
			Console.WriteLine("[{0,5}][crash] On piece {1}", gameTick, m_currentPieceIndex);
			Console.WriteLine("        PIECE - Radius: {0}, Angle: {1}", piece.radius, piece.angle);
			Console.WriteLine("        CAR   - Speed: {0}, Angle: {1}", m_currentSpeed, m_currentAngle);
			m_totalCrashes++;

			Suspension.OnCrash(m_currentAngle);

			m_crashed = true;
		}
	}

	protected void ProcessSpawn(JObject msg, int gameTick)
	{
		string name = (string)msg["name"];
		if (name == m_carName)
		{
			Console.WriteLine("[{0,5}][spawn]", gameTick);
			m_totalSpawns++;
			
			// Reset car parts
			Engine.OnSpawn();
			Suspension.OnSpawn();

			// Reset AI state
			m_currentAngle = 0;
			m_currentSpeed = 0;
			m_currentThrottle = 0;

			m_crashed = false;
		}
	}

	protected void ProcessTurbo(JObject msg, int gameTick)
	{
		int duration = (int)msg["turboDurationTicks"];
		double factor = (double)msg["turboFactor"];

		Console.WriteLine("[{0,5}][turboAvailable] Factor {1} duration {2}.", gameTick, factor, duration);
		if (factor > 1)
			Engine.OnTurboAvailable(duration, factor);
	}

	protected void ProcessTurboStart(JObject msg, int gameTick)
	{
		Console.WriteLine("[{0,5}][turboStart]", gameTick);
		Engine.OnTurboStart();
	}

	protected void ProcessTurboEnd(JObject msg, int gameTick)
	{
		Console.WriteLine("[{0,5}][turboEnd]", gameTick);
		Engine.OnTurboStop();
	}



	// Car data
	protected string m_carName;
	protected string m_carColor;
	
	// Track data
	private Track m_track;
	protected Track Track { get { return m_track; } }

	// Race state data
	private RaceStage m_raceStage = RaceStage.Race;
	protected RaceStage ActiveRaceStage { get { return m_raceStage; } }
	
	private int m_qualificationDurationMs = 0;
	protected bool QualificationStageAvailable { get { return m_qualificationDurationMs > 0; } }

	// Car state data
	private Engine m_engine;
	public Engine Engine { get { return m_engine; } }

	private Suspension m_suspension = new Suspension();
	public Suspension Suspension { get { return m_suspension; } }



	// AI
	private bool m_initTickDone = false;
	private State m_state = State.Telemetry_EnginePower;

	// Telemetry (gathered) data
	private int m_totalCrashes = 0;
	private int m_totalSpawns = 0;
	private int m_totalLaps = 0;
	private double m_teleEngineGain = double.NaN;
	
	private bool m_crashed = false;
	public bool Crashed { get { return m_crashed; } }

	// Runtime data
	private JArray m_lastCarPositions;
	private double m_currentSpeed;
	private double m_currentAngle;
	private double m_currentAngleD;
	private double m_currentAngleDD;
	private int m_currentPieceIndex;
	private double m_currentThrottle;

	// Display
	private BoxWriter m_tickBox = !Bot.MainBuild ? new BoxWriter("[{0,5}]", 10) : null;
	private BoxWriter m_speedBox = !Bot.MainBuild ? new BoxWriter("Speed: {0:00.0##}", 10) : null;
	private BoxWriter m_throttleBox = !Bot.MainBuild ? new BoxWriter("Throttle: {0:0.0##}", 10) : null;
	private BoxWriter m_angleBox = !Bot.MainBuild ? new BoxWriter("Angle: {0:000.0##}", 10) : null;

	private SendMsg ProcessTick(JArray carPositions, bool initialization, int gameTick)
	{
		if (initialization)
		{
			if (!Bot.MainBuild)
			{
				if (Console.BufferWidth < 120)
				{
					// Increase buffer to fit BoxWriters
					Console.SetBufferSize(120, Console.BufferHeight);
				}
			}

			m_lastCarPositions = carPositions;
			return OnInitTick(carPositions, gameTick);
		}



		SendMsg retMsg = null;

		double lastSpeed = m_currentSpeed;
		double lastAngle = m_currentAngle;
		double lastAngleD = m_currentAngleD;
		double lastAngleDD = m_currentAngleDD;
		int lastPieceIndex = m_currentPieceIndex;

		JToken carPosition = GetCarPosition(carPositions);
		JToken carPiecePos = carPosition["piecePosition"];
		m_currentSpeed = (m_lastCarPositions == null) ? 0.0f : CalculateSpeed(carPiecePos, GetCarPosition(m_lastCarPositions)["piecePosition"]);
		m_currentAngle = (double)carPosition["angle"];
		m_currentAngleD = m_currentAngle - lastAngle;
		m_currentAngleDD = m_currentAngleD - lastAngleD;
		m_currentPieceIndex = (int)carPiecePos["pieceIndex"];

		// If we had qualification, then begin race in State.Race mode. All data must be known by then.
		if (QualificationStageAvailable && ActiveRaceStage == RaceStage.Race)
		{
			if (m_state != State.Race)
			{
				Console.WriteLine("Warning. AI was in {0} state at the start of the race! Qualification probably failed!", m_state);
				m_state = State.Race;
			}
		}

		// AI states
		if (m_state == State.Telemetry_EnginePower)
		{
			retMsg = new Throttle(1.0);

			if (m_currentSpeed > 0.0)
			{
				m_teleEngineGain = m_currentSpeed;
				Console.WriteLine("[{0,5}][TELE] Found engine gain: {1}", gameTick, m_teleEngineGain);
				m_state = State.Telemetry_Friction;
			}
		}
		else if (m_state == State.Telemetry_Friction)
		{
			if (lastSpeed != m_teleEngineGain)
				throw new Exception("Telemetry failed");
			double engineFriction = -(m_currentSpeed - lastSpeed - m_teleEngineGain) / (m_teleEngineGain);
			Console.WriteLine("[{0,5}][TELE] Found friction: {1}", gameTick, engineFriction);

			m_engine = new Engine(m_teleEngineGain, engineFriction);

			m_state = State.Telemetry_Sliding;
		}
		else if (m_state == State.Telemetry_Sliding)
		{
			// Log first valid slip
			double eps = 0.000001;
			if (Math.Abs(lastAngleDD) < eps && Math.Abs(lastAngleD) < eps && Math.Abs(lastAngle) < eps &&
				Math.Abs(m_currentAngleDD) > 0.0 && !Crashed)
			{
				Piece piece = Track.Pieces[m_currentPieceIndex];
				int currentLane = (int)carPiecePos["lane"]["startLaneIndex"];
				double radius = piece.IsCorner ? Track.GetPieceRadiusForLane(piece, currentLane) : 0;

				Console.WriteLine("[{0,5}][TELE] First slip. Speed: {1:00.0#####}, Radius: {2:0000.0#}", gameTick, m_currentSpeed, radius);
				Console.WriteLine("              Angular acceleration: {0}", m_currentAngleDD);

				Suspension.CalculateSlipTelemetry(m_currentSpeed, radius, m_currentAngleDD);
				Console.WriteLine("              Max centrifugal force: {0}", Suspension.MaxCentrifugal);

				m_state = State.Race;
			}
		}
		
		// Primary state, can be jumped to immediately
		if (m_state == State.Race)
		{
			retMsg = OnRaceTick(m_lastCarPositions, carPositions, gameTick);
		}


		
		// Write stats
		if (m_tickBox != null)
		{
			m_tickBox.PushNewValue(gameTick);
			m_tickBox.Draw(60, 3);
		}

		if (m_speedBox != null)
		{
			m_speedBox.PushNewValue(m_currentSpeed);
			m_speedBox.Draw(68, 3);
		}

		if (m_throttleBox != null)
		{
			m_throttleBox.PushNewValue(m_currentThrottle);
			m_throttleBox.Draw(82, 3);
		}

		if (m_angleBox != null)
		{
			m_angleBox.PushNewValue(m_currentAngle);
			m_angleBox.Draw(98, 3);
		}

		// Update car components
		if (m_engine != null)
			m_engine.OnEndTick(gameTick, m_currentThrottle);

		// Intercept throttle
		if (retMsg is Throttle)
			m_currentThrottle = ((Throttle)retMsg).value;

		// Preserve last state
		m_lastCarPositions = carPositions;

		// Ping if no message created
		return (retMsg == null) ? new Ping() : retMsg;
	}

	/// <summary>
	/// Process the very first tick before a race.
	/// </summary>
	protected abstract SendMsg OnInitTick(JArray currCarPositions, int gameTick);

	/// <summary>
	/// Process each race tick.
	/// </summary>
	/// <param name="prevCarPositions">Previous car positions (json)</param>
	/// <param name="currCarPositions">Current car positions (json)</param>
	/// <param name="gameTick">Current game tick</param>
	/// <returns>Return a server message or null</returns>
	protected abstract SendMsg OnRaceTick(JArray prevCarPositions, JArray currCarPositions, int gameTick);

	/// <summary>
	/// Extracts OUR carPosition from json array.
	/// </summary>
	protected JToken GetCarPosition(JArray carPositions)
	{
		return GetCarPosition(carPositions, m_carName);
	}

	/// <summary>
	/// Extracts any carPosition from json array.
	/// </summary>
	protected JToken GetCarPosition(JArray carPositions, string name)
	{
		return carPositions.Where(o => (string)o["id"]["name"] == name).First();
	}

	//TODO:deserves a refactor
	protected double CalculateSpeed(JToken thisCarPiecePos, JToken prevCarPiecePos)
	{
		double speed = 0.0f;

		int pieceIndex = (int)thisCarPiecePos["pieceIndex"];
		double inPieceDistance = (double)thisCarPiecePos["inPieceDistance"];
		int startLaneIndex = (int)thisCarPiecePos["lane"]["startLaneIndex"];
		int endLaneIndex = (int)thisCarPiecePos["lane"]["endLaneIndex"];

		int lastPieceIndex = (int)prevCarPiecePos["pieceIndex"];
		double lastInPieceDistance = (double)prevCarPiecePos["inPieceDistance"];

		if (lastPieceIndex == pieceIndex)
		{
			// Still same piece
			speed = inPieceDistance - lastInPieceDistance;
		}
		else
		{
			// Next piece
			//TODO:can we go fast enough to skip a piece?
			speed = m_track.GetPieceLength(m_track.Pieces[lastPieceIndex], startLaneIndex, endLaneIndex) - lastInPieceDistance;
			speed += inPieceDistance;
		}

		return speed;
	}
}
