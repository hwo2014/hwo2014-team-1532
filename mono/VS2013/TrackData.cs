﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class TrackData
{
	public Dictionary<int, List<TrackRangeData>> m_Data = new Dictionary<int,List<TrackRangeData>>();

	public void AddSample (int rangeIndex, double entryangle, double entryspeed, double maxAngle)
	{
		if (!m_Data.ContainsKey(rangeIndex))
			m_Data.Add(rangeIndex, new List<TrackRangeData>());

		m_Data[rangeIndex].Add(new TrackRangeData(entryangle, entryspeed, maxAngle));
	}

	public bool HasData (int rangeIndex)
	{
		return m_Data.ContainsKey(rangeIndex);
	}

	public double GetLastTargetSpeed (int rangeIndex)
	{
		return m_Data[rangeIndex].Last().m_TargetSpeed;
	}

	public double GetLastDrift (int rangeIndex)
	{
		return m_Data[rangeIndex].Last().m_MaxAngle;
	}

	public double GetLastEntryAngle(int rangeIndex)
	{
		return m_Data[rangeIndex].Last().m_MaxAngle;
	}
}

public class TrackRangeData
{
	public double m_EntryAngle;
	public double m_TargetSpeed;
	public double m_MaxAngle;

	public TrackRangeData (double entryangle, double targetSpeed, double maxAngle)
	{
		m_EntryAngle = entryangle;
		m_TargetSpeed = targetSpeed;
		m_MaxAngle = maxAngle;
	}
}

