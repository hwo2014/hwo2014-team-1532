﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class CarState
{
    private Track m_Track;
    private double m_Throttle;
    private List<TrackPosition> m_TrackPositions;

    public TrackPosition CurrentTrackPosition { get { return m_TrackPositions.Last(); } }
    public TrackPosition PreviousTrackPosition { get { return m_TrackPositions.Count > 1 ? m_TrackPositions[m_TrackPositions.Count - 2] : CurrentTrackPosition; } }
    private TrackPosition PreviousPreviousTrackPosition { get { return m_TrackPositions.Count > 2 ? m_TrackPositions[m_TrackPositions.Count - 3] : PreviousTrackPosition; } }
	
    public int Lane { get { return CurrentTrackPosition.m_Lane; } }
    public int PieceIndex { get { return CurrentTrackPosition.m_Index; } }
    public double InPieceDistance { get { return CurrentTrackPosition.m_InPieceDistance; } }
    public Piece CurrentPiece { get { return m_Track.Pieces[PieceIndex]; } }

	public double AngularAcceleration { get { return AngularSpeed - PreviousAngularSpeed; } }
	public double AngularSpeed { get { return CurrentTrackPosition.m_Angle - PreviousTrackPosition.m_Angle; } }
    public double AngularSpeedRad { get { return AngularSpeed * (Math.PI / 180.0); } }
    public double PreviousAngularSpeed { get { return PreviousTrackPosition.m_Angle - PreviousPreviousTrackPosition.m_Angle; } }
	public double PreviousPreviousAngularSpeed { get { return PreviousTrackPosition.m_Angle - PreviousPreviousTrackPosition.m_Angle; } }

	public double PreviousSpeed { get { return m_Track.GetDistanceBetween(PreviousPreviousTrackPosition, PreviousTrackPosition); } }
    public double Speed { get { return m_Track.GetDistanceBetween(PreviousTrackPosition, CurrentTrackPosition); } 	}

    public double Angle { get { return CurrentTrackPosition.m_Angle; } }

    public double Throttle { get { return m_Throttle; } set { m_Throttle = value; } }

    public CarState(Track track)
    {
        m_Track = track;
        m_TrackPositions = new List<TrackPosition>();
    }

	public int maxTick { get { return m_TrackPositions.Count(); } }

    public void AddNewTrackPosition(TrackPosition trackPosition)
    {
        m_TrackPositions.Add (trackPosition);
    }

	public TrackPosition GetTrackPosition(int tick)
	{
		return m_TrackPositions[tick];
	}

	// Rewind back to the start of this piece
	public int RewindToTick(int pieceIndex)
	{
		int tick = m_TrackPositions.Count - 1;
		TrackPosition current = m_TrackPositions[tick];

		while (current.m_Index != pieceIndex && tick != 0)
		{
			tick--;
			current = m_TrackPositions[tick];
		}
		while (current.m_Index == pieceIndex && tick != 0)
		{
			tick--;
			current = m_TrackPositions[tick];
		}
		return tick + 1;
	}

	public double GetMaxDriftBetween (int fromTick, int toTick)
	{
		double maxDrift = 0.0;
		for (int tick = fromTick; tick <= toTick; tick++)
		{
			double drift = m_TrackPositions[tick].m_Angle;
			if (Math.Abs(drift) > Math.Abs(maxDrift))
				maxDrift = drift;
		}
		return maxDrift;
	}
}

