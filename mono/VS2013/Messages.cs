﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class IncomingMsgWrapper
{
	public string msgType;
	public Object data;
	
	public string gameId = null;
	public int gameTick = -1;
}

public class MsgWrapper
{
	public string msgType;
	public Object data;

	public MsgWrapper(string msgType, Object data)
	{
		this.msgType = msgType;
		this.data = data;
	}
}

public abstract class SendMsg
{
	public string ToJson()
	{
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}

	protected virtual Object MsgData()
	{
		return this;
	}

	protected abstract string MsgType();
}

class Turbo : SendMsg
{
	public string taunt;

	public Turbo(string taunt)
	{
		this.taunt = taunt;
	}

	protected override Object MsgData()
	{
		return taunt;
	}

	protected override string MsgType()
	{
		return "turbo";
	}
}


class Join : SendMsg
{
	public string name;
	public string key;
	public string color;

	public Join(string name, string key, string color = "red")
	{
		this.name = name;
		this.key = key;
		this.color = color;
	}

	protected override string MsgType()
	{
		return "join";
	}
}

class BotId
{
	public string name;
	public string key;
	public string color;
}

class JoinRaceSolo : SendMsg
{
	public BotId botId = new BotId();
	public string trackName;
	public string password;
	public int carCount;

	public JoinRaceSolo(string name, string key, string trackName, string color = "red")
	{
		botId.name = name;
		botId.key = key;
		botId.color = color;
		this.trackName = trackName;

		this.password = "dummy";
		this.carCount = 1;
	}

	protected override string MsgType()
	{
		return "joinRace";
	}
}

class JoinRace : SendMsg
{
	public BotId botId = new BotId();
	public string trackName;
	public string password;
	public int carCount;

	public JoinRace(string name, string key, string trackName, string password, int carCount, string color = null)
	{
		botId.name = name;
		botId.key = key;
		botId.color = color;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}

	protected override string MsgType()
	{
		return "joinRace";
	}
}

class Ping : SendMsg
{
	protected override string MsgType()
	{
		return "ping";
	}
}

class Throttle : SendMsg
{
	public double value;

	public Throttle(double value)
	{
		this.value = value;
	}

	protected override Object MsgData()
	{
		return this.value;
	}

	protected override string MsgType()
	{
		return "throttle";
	}
}

class SwitchLane : SendMsg
{
	public enum Direction
	{
		Left,
		Right,
		Nope
	}

	public Direction value;

	public SwitchLane(Direction value)
	{
		if (value == Direction.Nope)
			throw new System.Exception("This value is not for sending");
		this.value = value;
	}

	protected override Object MsgData()
	{
		return this.value.ToString();
	}

	protected override string MsgType()
	{
		return "switchLane";
	}
}
