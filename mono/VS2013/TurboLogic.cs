﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


class TurboLogic
{
	public static SendMsg HandleTurbo(Track track, Engine engine, int currentPieceIndex)
	{
		if (!engine.TurboActive && engine.TurboAvailable)
		{
			int index = track.FindStartIndexOfLongestStraight();

			if (currentPieceIndex == index)
				return engine.ActivateTurbo();
		}
		return null;
	}
}

