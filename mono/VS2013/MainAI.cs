﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

class MainAI : AI
{
	// Runtime data
	private CarState m_CarState;

	// temp vars
	private double m_MaxDriftSinceLastApex;
	private int m_LaneOnLastApex;
	private double m_PreviousAngularAcceleration;

	public double Throttle { get { return m_CarState.Throttle; } set { m_CarState.Throttle = value; } }
	public double AngularAcceleration2 { get { return m_CarState.AngularAcceleration; } }
	public double AngularAcceleration { get { return m_CarState.AngularAcceleration; } }
	public double AngularSpeed { get { return m_CarState.AngularSpeed; } }
	public double PreviousSpeed { get { return m_CarState.PreviousSpeed; } }
	public double Speed { get { return m_CarState.Speed; } }
	public double Angle { get { return m_CarState.Angle; } }
	public int Lane { get { return m_CarState.Lane; } }
	public int PieceIndex { get { return m_CarState.PieceIndex; } }
	public double InPieceDistance { get { return m_CarState.InPieceDistance; } }
	public Piece CurrentPiece { get { return m_CarState.CurrentPiece; } }
	public Piece NextPiece { get { return Track.GetNextPiece(PieceIndex); } }
	public int NextPieceIndex { get { return Track.GetNextPieceIndex(PieceIndex); } }
	public TrackPosition CurrentPosition { get { return m_CarState.CurrentTrackPosition; } }
	public TrackPosition PrevPosition { get { return m_CarState.PreviousTrackPosition; } }

	private bool m_SwitchingEnabled = true;

	private double[,] apexTargetSpeeds;

	protected override SendMsg OnInitTick(JArray currCarPositions, int gameTick)
	{
		m_CarState = new CarState(Track);
		return null;
	}

	private void InitializeApexTargetSpeeds()
	{
		apexTargetSpeeds = new double[Track.Apexes.Count, Track.LaneCount];
		
		for (int a = 0; a < Track.Apexes.Count; a++)
		{
			Apex apex = Track.Apexes[a];
			for (int l = 0; l < Track.LaneCount; l++)
			{
				apexTargetSpeeds[a, l] = CalculateMaxSpeed(apex, l);
			}
		}
	}

	protected override SendMsg OnRaceTick(JArray prevCarPositions, JArray currCarPositions, int gameTick)
	{
		if (apexTargetSpeeds == null)
			InitializeApexTargetSpeeds();
		
		SendMsg retMsg = null;
		m_CarState.AddNewTrackPosition(TrackPosition.DecodeTrackPosition(GetCarPosition(currCarPositions)));

		int nextApexIndex = Track.GetNextApexIndex(PieceIndex, InPieceDistance, Lane);
		Apex nextApex = Track.Apexes[nextApexIndex];
		int nextApexTargetLane = TargetLane(nextApex.m_PieceIndex);
		TrackPosition nextApexPosition = Track.ApexToTrackPosition(nextApex, nextApexTargetLane);

		double distanceToNextApex = Track.GetDistanceBetween(PieceIndex, InPieceDistance, Lane, nextApexPosition.m_Index, nextApexPosition.m_InPieceDistance, nextApexTargetLane);
		double distanceToNextCurve = Track.GetDistanceBetween(PieceIndex, InPieceDistance, Lane, nextApex.m_StartPieceIndex, 0.0, TargetLane(nextApex.m_StartPieceIndex));
		double maxSpeedForNextApex = GetMaxSpeed(nextApexIndex, nextApexTargetLane);

		double targetSpeed = 100.0;
		if (distanceToNextApex < distanceToNextCurve)
		{
			targetSpeed = maxSpeedForNextApex;
		}
		else
		{
			targetSpeed = AIUtility.GetMaxSpeed(Speed, Engine, distanceToNextCurve, maxSpeedForNextApex);
		}

		if (Math.Abs(ExtrapolateAngle(Angle, AngularSpeed, AngularAcceleration, AngularAcceleration - m_PreviousAngularAcceleration, 15)) > Suspension.MaxAngle ||
			Math.Abs(ExtrapolateAngle(Angle, AngularSpeed, AngularAcceleration, 0.0, 12)) > Suspension.MaxAngle)
		{
			DataLogger.LogMessage("emergency", 1.0);
			targetSpeed = 0.0;
		}

		m_PreviousAngularAcceleration = AngularAcceleration;

		Throttle = Engine.GetThrottleToAccelerateInOneTick(Speed, targetSpeed);

		retMsg = new Throttle(Throttle);
		retMsg = HandleSwitching() ?? retMsg;
		retMsg = TurboLogic.HandleTurbo(Track, Engine, PieceIndex) ?? retMsg;

		m_MaxDriftSinceLastApex = Math.Max(Math.Abs(Angle), m_MaxDriftSinceLastApex);

		if (ApexHasChanged())
			OnApexChange(gameTick);

		DataLogger.LogMessage("extrapolate", Math.Min (Suspension.MaxAngle, Math.Abs(ExtrapolateAngle(Angle, AngularSpeed, AngularAcceleration, AngularAcceleration - m_PreviousAngularAcceleration, 12))));
		DataLogger.LogMessage("anglespeed", m_CarState.AngularSpeed);
		DataLogger.LogMessage("distanceToNextCurve", distanceToNextCurve);
		DataLogger.LogMessage("distanceToNextApex", distanceToNextApex);
		DataLogger.LogMessage("speed", Speed);
		DataLogger.LogMessage("angle", Angle);
		DataLogger.LogMessage("radius", CurrentPiece.IsCorner ? CurrentPiece.radius : 500.0);
		DataLogger.LogMessage("currentIndex", PieceIndex);
		DataLogger.LogMessage("throttle", Throttle);
		DataLogger.LogMessage("targetSpeed", targetSpeed);
		DataLogger.LogMessage("maxSpeedForNextApex", maxSpeedForNextApex);

		return retMsg;
	}

	private void OnApexChange(int tick)
	{
		int prevApexIndex = Track.GetPrevApexIndex(PieceIndex, InPieceDistance, Lane);

		double driftingFail = 53.0 - m_MaxDriftSinceLastApex;
		
		if (tick > 100)
			apexTargetSpeeds[prevApexIndex, m_LaneOnLastApex] += driftingFail * 0.01;

		m_MaxDriftSinceLastApex = 0.0;
		m_LaneOnLastApex = Lane;
	}

	private double GetMaxSpeed(int apex, int lane)
	{
		return apexTargetSpeeds[apex, lane];
	}

	private SendMsg HandleSwitching()
	{
		SendMsg result = null;

		if (!m_SwitchingEnabled && m_CarState.PreviousTrackPosition.m_Lane != m_CarState.CurrentTrackPosition.m_Lane)
			m_SwitchingEnabled = true;

		if (m_SwitchingEnabled && NextPiece.switcher)
			result = SwitchingLogic.SwitchingLogicToMsg(SwitchingLogic.SwitchLogic(Track, NextPieceIndex, Lane));

		if (result != null)
			m_SwitchingEnabled = false;

		return result;
	}

	private double CalculateMaxSpeed(Apex apex, int lane)
	{
		//double longCurveAdjust = Math.Min(1.0, (90 + 600) / (apex.m_TotalAngle + 600));
		double engineGainAdjust = (0.2 + 1) / (Engine.EnginePower + 1);
		double engineFrictionAdjust = (Engine.Friction + 0.1) / (0.02 + 0.1);
		return Suspension.GetMaxSpeed(Track.GetPieceRadiusForLane(Track.Pieces[apex.m_PieceIndex], lane)) * engineGainAdjust * engineFrictionAdjust * 0.98;
	}

	// Given current position, what is our lane going to be in toIndex
	private int TargetLane(int toIndex)
	{
		return SwitchingLogic.TargetLane(Track, PieceIndex, Lane, toIndex);
	}

	private bool ApexHasChanged()
	{
		return Track.GetNextApexIndex(PrevPosition.m_Index, PrevPosition.m_InPieceDistance, PrevPosition.m_Lane) != Track.GetNextApexIndex(CurrentPosition.m_Index, CurrentPosition.m_InPieceDistance, CurrentPosition.m_Lane);
	}

	private double ExtrapolateAngle(double currentAngle, double angleVelocity, double angleAcceleration, double angleAcceleration2, int ticks)
	{
		double angle = currentAngle;
		double dA = angleVelocity;
		double ddA = angleAcceleration;
		double dddA = angleAcceleration2;

		for (int i=ticks; i>0; i--)
		{
			angle += dA;
			dA += ddA;
			ddA += dddA;
		}

		return angle;
	}
}

