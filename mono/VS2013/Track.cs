using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Piece
{
	public double length = 0;
	public double radius = 0;
	public double angle = 0;
	public bool switcher = false;

	public bool IsCorner { get { return angle != 0; } }
	public bool IsTurnRight { get { return angle > 0; } }
	public bool IsTurnLeft { get { return angle < 0; } }
	
	public Piece(JToken init)
	{
		if (init["angle"] != null)
		{
			radius = (double)init["radius"];
			angle = (double)init["angle"];
		}
		else
		{
			length = (double)init["length"];
		}
		if (init["switch"] != null)
			switcher = (bool)init["switch"];
	}

	public bool SameRegion(Piece other)
	{
		return (this.radius == other.radius) && Math.Sign(this.angle) == Math.Sign(other.angle) && !this.switcher && !other.switcher;
	}
}

public class Track
{
	private double[] m_lanes = new double[Specs.MaxLanes] { double.NaN, double.NaN, double.NaN, double.NaN } ;
	public double[] Lanes { get { return m_lanes; } }

	private List<Piece> m_pieces = new List<Piece>();
	public List<Piece> Pieces { get { return m_pieces; } }

	private List<PieceRange> m_ranges = new List<PieceRange>();
	public List<PieceRange> Ranges { get { return m_ranges; } }

	private List<Apex> m_Apexes = new List<Apex>();
	public List<Apex> Apexes { get { return m_Apexes; } }
	
	/// <param name="side">-1 for left, +1 for right</param>
	public bool HasLaneToTheSide(int laneIndex, int side, out int newLaneIndex)
	{
		int newLane = laneIndex + side;
		if (newLane >= 0 && newLane < Specs.MaxLanes && !double.IsNaN(m_lanes[newLane]))
		{
			newLaneIndex = newLane;
			return true;
		}
		else
		{
			newLaneIndex = -1;
			return false;
		}
	}

    public int LaneCount 
    { 
        get 
        {
            int result = 0;
            for (; result < m_lanes.Length; result++)
            {
                if (double.IsNaN(Lanes[result]))
                    return result;
            }

            return result;
        } 
    }

	/// <summary>
	/// Returns the total length of a lane on a piece of track
	/// </summary>
	/// <param name="piece">Track piece</param>
	/// <param name="laneIndex">Lane index</param>
	public double GetPieceLength(Piece piece, int laneIndex, int targetLaneIndex)
	{
		if (piece.IsCorner)
		{
			if (laneIndex != targetLaneIndex)
			{
				double laneDistance = Math.Abs(Lanes[laneIndex] - Lanes[targetLaneIndex]);
				double radius = (GetPieceRadiusForLane(piece, laneIndex) + GetPieceRadiusForLane(piece, targetLaneIndex)) * .5f;
				double len = (2 * Math.PI * radius * Math.Abs(piece.angle) / 360.0);
				return Math.Sqrt(len * len + laneDistance * laneDistance);
			}
			return (2 * Math.PI * GetPieceRadiusForLane(piece, laneIndex)) * (Math.Abs(piece.angle) / 360.0);
		}
		else
		{
			if (laneIndex != targetLaneIndex)
			{
				double laneDistance = Math.Abs(Lanes[laneIndex] - Lanes[targetLaneIndex]);
				return Math.Sqrt (piece.length * piece.length + laneDistance * laneDistance);
			}
			return piece.length;
		}
	}

	public double GetPieceRadiusForLane(Piece piece, int laneIndex)
	{
		if (!piece.IsCorner)
			throw new Exception("BUG");
		double radius = piece.radius;
		double lane = m_lanes[laneIndex];
		radius += piece.IsTurnRight ? -lane : lane;
		return radius;
	}

	public int GetPrevPieceIndex(int fromPieceIndex)
	{
		int prevPieceIndex = fromPieceIndex - 1;
		if (prevPieceIndex < 0)
			prevPieceIndex = m_pieces.Count - 1;
		return prevPieceIndex;
	}

	public int GetNextPieceIndex(int fromPieceIndex)
	{
		int nextPieceIndex = fromPieceIndex + 1;
		if (nextPieceIndex >= m_pieces.Count)
			nextPieceIndex = 0;
		return nextPieceIndex;
	}

	public int GetPieceIndex (int fromPieceIndex, int offset)
	{
		int i = 0;
		int steps = Math.Abs(offset);
		int result = fromPieceIndex;

		while (i < steps)
		{
			if (offset > 0)
				result = GetNextPieceIndex (result);
			else
				result = GetPrevPieceIndex (result);
			i++;
		}

		return result;
	}

	public int GetIndexDistance (int fromPieceIndex, int toPieceIndex)
	{
		int currentIndex = fromPieceIndex;
		int distance = 0;

		while (currentIndex != toPieceIndex)
		{
			distance++;
			currentIndex = GetNextPieceIndex(currentIndex);
		}

		return distance;
	}

	public Piece GetPrevPiece(int fromPieceIndex)
	{
		return m_pieces[GetPrevPieceIndex(fromPieceIndex)];
	}

	public Piece GetNextPiece(int fromPieceIndex)
	{
		return m_pieces[GetNextPieceIndex(fromPieceIndex)];
	}

    public int FindStartIndexOfLongestStraight()
    {
        int firstStraight = GetNextStraightIndex(GetNextCornerIndex(0));
        int currentIndex = firstStraight;
        int nextCorner = GetNextCornerIndex(currentIndex);
        double longestStraight = GetDistanceBetween(currentIndex, nextCorner, 0);
        int longestStraightIndex = firstStraight;
        currentIndex = GetNextStraightIndex(nextCorner);

        while (currentIndex != firstStraight)
        {
            nextCorner = GetNextCornerIndex (currentIndex);
            double thisStraight = GetDistanceBetween(currentIndex, nextCorner, 0);
            if (longestStraight < thisStraight)
            {
                longestStraightIndex = currentIndex;
                longestStraight = thisStraight;
            }
            currentIndex = GetNextStraightIndex (nextCorner);
        }

        return longestStraightIndex;
    }
    
	/// <summary>
	/// Finds distance to the next piece with a different angle
	/// </summary>
	public double GetDistanceToNextAngle(int fromPieceIndex, int laneIndex, double inPieceDistance, out Piece nextPiece, out int switchesAvailable)
	{
		switchesAvailable = 0;

		Piece fromPiece = m_pieces[fromPieceIndex];
		double distance = GetPieceLength(fromPiece, laneIndex, laneIndex) - inPieceDistance; // Distance to the very next piece

		//TODO:this code is slow!
		nextPiece = GetNextPiece(fromPieceIndex);
		while (fromPiece.angle == nextPiece.angle) //TODO:OLD logic, has issues
		{
			distance += GetPieceLength(nextPiece, laneIndex, laneIndex);
			if (nextPiece.switcher)
				++switchesAvailable;
			
			fromPieceIndex = GetNextPieceIndex(fromPieceIndex);
			nextPiece = GetNextPiece(fromPieceIndex);
		}

		if (nextPiece.switcher)
			++switchesAvailable;

		return distance;
	}

	/*public int GetTicksToCoverDistanceNoSlip(int fromIndex, int toIndex, int lane, double maxStartSpeed, AI ai)
	{
		int ticks = 0;
		double actualSpeed = maxStartSpeed;

		for (int i = fromIndex; i < toIndex; ++i)
		{
			Piece piece = Pieces[i];
			double distance = GetPieceLength(piece, lane, lane);

			double maxSpeedInNextPiece = GetMaxEntrySpeedWithLookAhead(ai, GetNextPieceIndex(i), lane, 25); //TODO:not accurate
			double maxSpeedThisPiece = AIUtility.GetMaxSpeed(actualSpeed, ai.Engine, distance, maxSpeedInNextPiece);
			if (actualSpeed > maxSpeedThisPiece)
				actualSpeed = maxSpeedThisPiece;
			while (distance > 0)
			{
				double throttle = ai.Engine.GetThrottleToAccelerateInOneTick(actualSpeed, maxSpeedThisPiece);
				double speed = ai.Engine.GetSpeedAfterOneTick(actualSpeed, throttle);
				distance -= (actualSpeed + speed) / 2;
				actualSpeed = speed;
				++ticks;
			}
		}

		return ticks;
	}*/

    /// <summary>
    /// Finds distance between two positions on the track on the same lane
    /// </summary>
    public double GetDistanceBetween(TrackPosition from, TrackPosition to, int lane)
    {
        return GetDistanceBetween(from.m_Index, from.m_InPieceDistance, to.m_Index, to.m_InPieceDistance, lane);
    }

    /// <summary>
    /// Finds distance between two positions on the track on the same lane
    /// </summary>
    public double GetDistanceBetween(int fromIndex, int toIndex, int lane)
    {
        return GetDistanceBetween(fromIndex, 0.0, toIndex, 0.0, lane);
    }

    /// <summary>
    /// Finds distance between two positions on the track on fixed lane
    /// </summary>
    public double GetDistanceBetween(int fromIndex, double fromInPieceDistance, int toIndex, double toInPieceDistance, int lane)
    {
        if (fromIndex == toIndex && fromInPieceDistance <= toInPieceDistance)
            return toInPieceDistance - fromInPieceDistance;

        double distance = GetPieceLength(Pieces[fromIndex], lane, lane) - fromInPieceDistance;

        int pieceIndex = GetNextPieceIndex(fromIndex);
        while (pieceIndex != toIndex)
        {
			distance += GetPieceLength(Pieces[pieceIndex], lane, lane);
            pieceIndex = GetNextPieceIndex(pieceIndex);
        }

        distance += toInPieceDistance;
        return distance;
    }

    /// <summary>
    /// Distance between two track positions. Use first available switches inbetween is assumed. On impossible route, approx value returned.
    /// </summary>
    public double GetDistanceBetween(int fromIndex, double fromInPieceDistance, int fromLane, int toIndex, double toInPieceDistance, int toLane)
    {
        int howManySwitchesNeeded = Math.Abs(fromLane - toLane);

        if (howManySwitchesNeeded > 0) // Switching required
        {
            if (fromIndex == toIndex)
                return toInPieceDistance - fromInPieceDistance;

            int currentIndex = fromIndex;
            int currentLane = fromLane;
            int switchDirection = fromLane < toLane ? 1 : -1;
            double currentInPieceDistance = fromInPieceDistance;
            double totalDistance = 0.0;

            for (int s = 0; s < howManySwitchesNeeded; s++)
            {
                int newSwitch = GetNextPossibleSwitchIndex(currentIndex);

                double distanceToSwitch = GetDistanceBetween(currentIndex, newSwitch, currentLane) - currentInPieceDistance;
				double distanceToTargetPos = GetDistanceBetween(currentIndex, toIndex, currentLane) + toInPieceDistance - currentInPieceDistance;

                if (distanceToSwitch < distanceToTargetPos) // Next switch is closer than our target position. Let's dowit.
                {
					currentIndex = GetNextPieceIndex (newSwitch);
                    int previousLane = currentLane;
					currentLane = Math.Max(Math.Min(currentLane + switchDirection, LaneCount - 1), 0);
                    currentInPieceDistance = 0.0;
					totalDistance += distanceToSwitch + GetPieceLength(Pieces[newSwitch], previousLane, currentLane);
				}
                else // Target is closer than the next switch. abort!
                {
					if (Pieces[currentIndex].switcher)
						totalDistance += GetPieceLength(Pieces[currentIndex], currentLane, toLane) - currentInPieceDistance + toInPieceDistance;
					else
						totalDistance += currentInPieceDistance;
					return totalDistance;
                }
            }

            totalDistance += GetDistanceBetween(currentIndex, currentInPieceDistance, toIndex, toInPieceDistance, currentLane); // Rest of the way
            return totalDistance;
        }
        else // No switching required
        {
            return GetDistanceBetween(fromIndex, fromInPieceDistance, toIndex, toInPieceDistance, toLane);
        }
    }

    /// <summary>
    /// Distance between two track positions. Use first available switches inbetween is assumed. On impossible route, approx value returned.
    /// </summary>
    public double GetDistanceBetween(TrackPosition from, TrackPosition to)
    {
        return GetDistanceBetween(from.m_Index, from.m_InPieceDistance, from.m_Lane, to.m_Index, to.m_InPieceDistance, to.m_Lane);
    }

    /// <summary>
    /// Returns next available switch. If fromIndex is a switch, it is ignored (too late to switch).
    /// </summary>
    public int GetNextPossibleSwitchIndex(int fromIndex)
    {
        int pieceIndex = fromIndex + 1;
        while (pieceIndex != fromIndex)
        {
            if (pieceIndex >= Pieces.Count)
                pieceIndex = 0;

            if (Pieces[pieceIndex].switcher)
                return pieceIndex;

            pieceIndex++;
        }

        // TODO: handle case with zero switch track
        return 0;
    }

    /// <summary>
    /// Returns index of the closest previous piece where corner started a.k.a straight or wider radius
    /// </summary>
    public int GetCornerStart(int cornerIndex)
    {
        Piece corner = Pieces[cornerIndex];
        int p = GetPrevPieceIndex(cornerIndex);
        while (p != cornerIndex)
        {
            if (!Pieces[p].IsCorner || Pieces[p].radius > corner.radius)
                return GetNextPieceIndex(p);

            p = GetPrevPieceIndex(p);
        }
        return p;
    }

    /// <summary>
    /// Returns index of the closest next piece where corner ends a.k.a straight or wider radius
    /// </summary>
    public int GetCornerEnd(int cornerIndex)
    {
        Piece corner = Pieces[cornerIndex];
        int p = GetNextPieceIndex(cornerIndex);
        while (p != cornerIndex)
        {
            if (!Pieces[p].IsCorner || Pieces[p].radius > corner.radius)
                return p;

            p = GetNextPieceIndex(p);
        }
        return p;
    }

	public double GetCornerApexDistanceFromStart (int cornerIndex, int lane)
	{
		Piece corner = Pieces[cornerIndex];
		int p = GetNextPieceIndex(cornerIndex);
		while (p != cornerIndex)
		{
			if (!Pieces[p].IsCorner || Pieces[p].radius > corner.radius)
				return p;

			p = GetNextPieceIndex(p);
		}
		return GetDistanceBetween(cornerIndex, p, lane) * .5f;
	}

    /// <summary>
    /// Returns index of the next closest corner
    /// </summary>
    public int GetNextCornerIndex(int currentIndex)
    {
        int p = GetNextPieceIndex(currentIndex);
        while (p != currentIndex)
        {
            if (Pieces[p].IsCorner)
                return p;

            p = GetNextPieceIndex(p);
        }
        return currentIndex;
    }

	/// <summary>
	/// Returns index of the start of the next logical corner, which is not part of current one. Next logical corner is one with opposite angle or straight between current.
	/// </summary>
	public int GetNextLogicalCornerIndex(int currentIndex)
	{
		int p = GetNextPieceIndex(currentIndex);
		Piece originalPiece = Pieces[currentIndex];

		if (!originalPiece.IsCorner)
			return GetNextCornerIndex(currentIndex);

		double originalRadius = Pieces[currentIndex].radius;
		double originalAngle = Pieces[currentIndex].angle;

		bool straightFound = false;
		while (p != currentIndex)
		{
			Piece piece = Pieces[p];
			if (!piece.IsCorner)
				straightFound = true;

			if (piece.IsCorner && (piece.angle * originalAngle < 0 || straightFound))
				return p;

			p = GetNextPieceIndex(p);
		}
		return currentIndex;
	}

    /// <summary>
    /// Returns index of the next corner that has big enough angle
    /// </summary>
    public int GetNextCornerIndex(int currentIndex, double minAngle)
    {
        int p = GetNextPieceIndex(currentIndex);
        while (p != currentIndex)
        {
            if (Pieces[p].IsCorner && Math.Abs(GetCornerRealAngle(p)) >= minAngle)
                return p;

            p = GetNextPieceIndex(p);
        }
        return currentIndex;
    }

    public double GetCornerRealAngle (int cornerStartIndex)
    {
        int cornerEnds = GetNextStraightIndex(cornerStartIndex);
        double totalAngle = 0.0;

        int p = cornerStartIndex;
        while (p != cornerEnds)
        {
            if (Pieces[p].IsCorner)
                totalAngle += Pieces[p].angle;

            p = GetNextPieceIndex(p);
        }
        return totalAngle;
    }

    /// <summary>
    /// Returns index of the next straight piece
    /// </summary>
    private int GetNextStraightIndex(int currentIndex)
    {
        int p = GetNextPieceIndex(currentIndex);
        while (p != currentIndex)
        {
            if (!Pieces[p].IsCorner)
                return p;

            p = GetNextPieceIndex(p);
        }
        return currentIndex;
    }

	public int GetRangeIndex (int currentPieceIndex)
	{
		for(int r=0; r<Ranges.Count; r++)
		{
			PieceRange range = Ranges[r];
			if ((range.m_StartIndex <= currentPieceIndex && range.m_EndIndex > currentPieceIndex) ||
				(range.m_StartIndex > range.m_EndIndex && (range.m_StartIndex <= currentPieceIndex || range.m_EndIndex > currentPieceIndex)))
			{
				return r;
			}
		}
		return 0;
	}

	public int GetPrevApexIndex(int currentPieceIndex, double currentInPieceDistance, int currentLane)
	{
		Piece currentPiece = Pieces[currentPieceIndex];
		double pieceLength = GetPieceLength(currentPiece, currentLane, currentLane);
		double normalizedDistance = currentInPieceDistance / pieceLength;
		return GetPrevApexIndexNormalized(currentPieceIndex, normalizedDistance);
	}

	public int GetNextApexIndex(int currentPieceIndex, double currentInPieceDistance, int currentLane)
	{
		Piece currentPiece = Pieces[currentPieceIndex];
		double pieceLength = GetPieceLength(currentPiece, currentLane, currentLane);
		double normalizedDistance = currentInPieceDistance / pieceLength;
		return GetNextApexIndexNormalized(currentPieceIndex, normalizedDistance);
	}

	public TrackPosition ApexToTrackPosition (Apex apex, int lane)
	{
		TrackPosition newPosition = new TrackPosition();
		newPosition.m_Lane = lane;
		newPosition.m_Index = apex.m_PieceIndex;
		newPosition.m_InPieceDistance = GetApexInPieceDistance (apex, lane);
		return newPosition;
	}

	public double GetApexInPieceDistance (Apex apex, int lane)
	{
		return apex.m_InPieceDistanceNormalized * GetPieceLength(Pieces[apex.m_PieceIndex], lane, lane);
	}

	private int GetPrevApexIndexNormalized(int currentPieceIndex, double currentInPieceDistanceNormalized)
	{
		for (int r = Apexes.Count - 1; r >= 0; r--)
		{
			Apex apex = Apexes[r];
			if (apex.m_PieceIndex < currentPieceIndex || apex.m_PieceIndex == currentPieceIndex && apex.m_InPieceDistanceNormalized < currentInPieceDistanceNormalized)
				return r;
		}
		return Apexes.Count - 1;
	}

	private int GetNextApexIndexNormalized(int currentPieceIndex, double currentInPieceDistanceNormalized)
	{
		for (int r = 0; r < Apexes.Count; r++)
		{
			Apex apex = Apexes[r];
			if (apex.m_PieceIndex > currentPieceIndex || apex.m_PieceIndex == currentPieceIndex && apex.m_InPieceDistanceNormalized > currentInPieceDistanceNormalized)
				return r;
		}
		return 0;
	}

	public int GetNextRangeIndex(int currentPieceIndex)
	{
		return GetNextRangeIndexInCycle(GetRangeIndex (currentPieceIndex));
	}

	public int GetNextRangeIndexInCycle(int currentRangeIndex)
	{
		return currentRangeIndex + 1 == Ranges.Count ? 0 : currentRangeIndex + 1;
	}

	public int GetPrevRangeIndexInCycle(int currentRangeIndex)
	{
		return currentRangeIndex - 1 < 0 ? Ranges.Count - 1 : currentRangeIndex - 1;
	}

	public void Initialize(JObject gameInit)
	{
		// Extract lane data
		foreach (var jLane in gameInit["race"]["track"]["lanes"].Children())
		{
			int index = (int)jLane["index"];
			double distance = (double)jLane["distanceFromCenter"];
			m_lanes[index] = distance;
		}

		// Extract track piece data
		foreach (var jPiece in gameInit["race"]["track"]["pieces"].Children())
		{
			m_pieces.Add(new Piece(jPiece));
		}

		InitializePieceRanges();
		InitializeApexes();
	}

	public void InitializePieceRanges()
	{
		int firstCorner = GetNextLogicalCornerIndex(0);
		int nextCorner = GetNextLogicalCornerIndex(firstCorner);
		m_ranges.Add (new PieceRange(firstCorner, nextCorner));
		int currentIndex = nextCorner;
		while (currentIndex != firstCorner)
		{
			nextCorner = GetNextLogicalCornerIndex(currentIndex);
			m_ranges.Add(new PieceRange(currentIndex, nextCorner));
			currentIndex = nextCorner;
		}
		return;
	}

	public void InitializeApexes()
	{
		int firstStraight = GetNextStraightIndex(GetNextCornerIndex(0));
		int currentIndex = firstStraight;
		int startOfSequence = currentIndex;

		currentIndex = GetNextPieceIndex(currentIndex);

		while (currentIndex != firstStraight)
		{	
			int previousIndex = GetPrevPieceIndex(currentIndex);

			Piece previousPiece = Pieces[previousIndex];
			Piece currentPiece = Pieces[currentIndex];

			double prevRadius = previousPiece.IsCorner ? previousPiece.radius : 0.0;
			double currRadius = currentPiece.IsCorner ? currentPiece.radius : 0.0;
			double prevAngle = previousPiece.IsCorner ? previousPiece.angle : 0.0;
			double currAngle = currentPiece.IsCorner ? currentPiece.angle : 0.0;

			if (previousPiece.IsCorner && (prevRadius != currRadius || previousPiece.angle * currentPiece.angle < 0))
				InsertApex(GetNewApex(startOfSequence, currentIndex));

			if (previousPiece.radius != currentPiece.radius || previousPiece.angle * currentPiece.angle < 0)
				startOfSequence = currentIndex;

			currentIndex = GetNextPieceIndex(currentIndex);
		}

		InsertApex(GetNewApex(startOfSequence, firstStraight));
	}

	private Apex GetNewApex (int curveStart, int curveEnd)
	{
		int distance = GetIndexDistance(curveStart, curveEnd);
		int apexPieceIndex = GetPieceIndex(curveStart, (int)Math.Floor(distance / 2.0));
		double apexInPieceDistanceNormalized = 0.0;
		if (distance % 2 == 1)
			apexInPieceDistanceNormalized = 0.5;

		double totalAngle = GetTotalAngle(curveStart, curveEnd);
		
		return new Apex(apexPieceIndex, curveStart, curveEnd, apexInPieceDistanceNormalized, totalAngle);
	}

	private double GetTotalAngle (int fromPiece, int toPiece)
	{
		int currentIndex = fromPiece;
		double totalAngle = 0.0;

		while (currentIndex != toPiece)
		{
			totalAngle += Pieces[currentIndex].IsCorner ? Math.Abs (Pieces[currentIndex].angle) : 0.0;
			currentIndex = GetNextPieceIndex(currentIndex);
		}

		return totalAngle;
	}

	private void InsertApex (Apex apex)
	{
		for (int r = 0; r < Apexes.Count; r++)
		{
			if (Apexes[r].m_PieceIndex > apex.m_PieceIndex)
			{
				Apexes.Insert(r, apex);
				return;
			}
		}
		Apexes.Add(apex);
	}

	/// <summary>
	/// Look up future rules
	/// </summary>
	/*public double GetMaxEntrySpeedWithLookAhead(AI ai, int pieceIndex, int laneIndex, int recurseDeep)
	{
		Piece piece = m_pieces[pieceIndex];
		double maxSpeed = ai.GetMaxSpeedForPiece(piece, laneIndex);
		
		if (recurseDeep > 0)
		{
			double distance = GetPieceLength(piece, laneIndex, laneIndex);
			double maxAhead = GetMaxEntrySpeedWithLookAhead(ai, GetNextPieceIndex(pieceIndex), laneIndex, recurseDeep - 1);
			double brakingDistance = ai.Engine.GetBrakingDistance(maxSpeed, maxAhead);
			if (brakingDistance > distance)
			{
				double maxEntry = maxAhead;
				while (distance > 0)
				{
					maxEntry = maxAhead / (1 - 1 * ai.Engine.Friction);
					distance -= maxEntry;//TODO:goes over the value!
				}

				maxSpeed = Math.Min(maxEntry, maxSpeed);
			}
		}

		return maxSpeed;
	}*/
}

public class Apex
{
	public int m_StartPieceIndex;
	public int m_EndPieceIndex;
	public int m_PieceIndex;
	public double m_InPieceDistanceNormalized;
	public double m_TotalAngle;

	public Apex(int pieceIndex, int startPieceIndex, int endPieceIndex, double inPieceDistanceNormalized, double totalAngle)
	{
		m_PieceIndex = pieceIndex;
		m_StartPieceIndex = startPieceIndex;
		m_EndPieceIndex = endPieceIndex;
		m_InPieceDistanceNormalized = inPieceDistanceNormalized;
		m_TotalAngle = totalAngle;
	}
}

public class PieceRange
{
	public int m_StartIndex;
	public int m_EndIndex;

	public PieceRange(int startIndex, int endIndex)
	{
		m_StartIndex = startIndex;
		m_EndIndex = endIndex;
	}
}

public class TrackPosition
{
    public int m_Index;
    public double m_InPieceDistance;
    public int m_Lane;
	public int m_TargetLane;
    public double m_Angle;

    /// <summary>
    /// Decode JSON into new TrackPosition data.
    /// </summary>
    public static TrackPosition DecodeTrackPosition(JToken carPosition)
    {
        TrackPosition trackPosition = new TrackPosition();

        JToken carPiecePos = carPosition["piecePosition"];
        JToken lane = carPiecePos["lane"];

        trackPosition.m_Lane = (int)lane["startLaneIndex"];
		trackPosition.m_TargetLane = (int)lane["endLaneIndex"];
		trackPosition.m_Index = (int)carPiecePos["pieceIndex"];
        trackPosition.m_InPieceDistance = (double)carPiecePos["inPieceDistance"];
        trackPosition.m_Angle = (double)carPosition["angle"];

        return trackPosition;
    }
}
