﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

class DataLogger
{
    public const string logFolderName = "logs";
    public const string logFileExtension = ".log";
    
    private static StreamWriter s_StreamWriter;
    private static int s_Tick;

    public DataLogger (string name)
    {
        string s = Path.DirectorySeparatorChar.ToString();
        string rootPath = Path.GetDirectoryName(Environment.CurrentDirectory);
        string logFolderPath = rootPath + s + logFolderName;
        string logfilePath = logFolderPath + s + name + logFileExtension;

        Directory.CreateDirectory (logFolderPath);
        s_StreamWriter = File.CreateText(logfilePath);
    }

    public void Close()
    {
        s_StreamWriter.Close();
    }

    public void OnReceiveMessage(string data)
    {
        if (s_StreamWriter == null)
            return;

        MsgWrapperWithGametick rcvMsg = JsonConvert.DeserializeObject<MsgWrapperWithGametick>(data);

        if (rcvMsg.msgType == "carPositions")
            s_Tick = rcvMsg.gameTick;
        
        s_StreamWriter.WriteLine(data);
    }

    public static void LogMessage(string name, double value)
    {
		if (s_StreamWriter != null)
			s_StreamWriter.WriteLine(JsonConvert.SerializeObject(new MsgWrapperWithGametick(name, value, s_Tick)));
    }


    public class MsgWrapperWithGametick
    {
        public string msgType;
        public Object data;
        public int gameTick;

        public MsgWrapperWithGametick(string msgType, Object data, int gameTick)
        {
            this.msgType = msgType;
            this.data = data;
            this.gameTick = gameTick;
        }
    }
}

