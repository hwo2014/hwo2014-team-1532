using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Suspension
{
	private double m_width = double.NaN;
	private double m_length = double.NaN;
	private double m_guideFlagPosition = double.NaN;

	// Sliding mechanics
	private double m_maxAngleDeg = 90; // Value is decreased on crash
	private double m_maxCentrifugal = double.NaN; // Max centrifugal force we can have before sliding

	public double MaxAngle { get { return m_maxAngleDeg; } }
	public double MaxCentrifugal { get { return m_maxCentrifugal; } }

	public Suspension()
	{
	}

	public double GetTopSpeedNoSlide(double radius)
	{
		// F=m*v*v/r
		// v = sqrt(F*r/m);
		return Math.Sqrt(m_maxCentrifugal * radius);
	}

	private double GetMaxSpeedCalc(double radius)
	{
		return Math.Sqrt(m_maxCentrifugal * radius * 1.2f); // TODO: how to find actual MAX speed?
	}

	public double GetMaxSpeed(double radius)
	{
		if (m_maxCentrifugal == double.NaN)
			throw new Exception("Telemetry stage failed");

		return GetMaxSpeedCalc(radius);
	}

	public void OnCrash(double angle)
	{
		angle = Math.Abs(angle);

		if (angle < m_maxAngleDeg)
		{
			Console.WriteLine("Suspension will now treat {0} as max slip angle (down from {1}).", angle, m_maxAngleDeg);
			m_maxAngleDeg = angle;
		}
		else
		{
			Console.WriteLine("Crashed at {0} which is above the known maximum {1}", angle, m_maxAngleDeg);
		}
	}

	public void OnSpawn()
	{

	}

	public void InitializeCar(double width, double length, double guideFlagPosition)
	{
		if (double.IsNaN(m_width))
		{
			m_width = width;
			m_length = length;
			m_guideFlagPosition = guideFlagPosition;
			Console.WriteLine("       Car size: {0} x {1}", m_width, m_length);
			Console.WriteLine("       Guide flag @ {0}", m_guideFlagPosition);
		}
	}

	public void CalculateSlipTelemetry(double speed, double cornerRadius, double angle)
	{
		// Current velocity in degrees per tick
		double cornerAngularVelocityDeg = AIUtility.Rad2Deg(speed / cornerRadius);

		// Maximum velocity in degrees per tick we can handle
		// The rest bleeds out linearly into the rotational system of the car
		double maxAngularVelocityDeg = cornerAngularVelocityDeg - angle;

		// Threshold speed
		double thresholdSpeed = maxAngularVelocityDeg / 360 * (2 * Math.PI * cornerRadius);

		// Threshold centrifugal force (F=m*v*v/r)
		m_maxCentrifugal = (thresholdSpeed * thresholdSpeed) / cornerRadius;
	}
}
