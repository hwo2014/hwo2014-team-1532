using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Bot
{
	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
#if LOCAL_BUILD_VS
	// Visual Studio sets the macro
	public static bool MainBuild = false;
#else
	public static bool MainBuild = true;
#endif
	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	public static void Main(string[] args)
	{
		string host = args[0];
		int port = int.Parse(args[1]);
		string botName = args[2];
		string botKey = args[3];
		if (!Bot.MainBuild)
		{
			Console.Clear();
			Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
		}

		using(TcpClient client = new TcpClient(host, port))
		{
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			// Create AI
			AI ai = new MainAI();

			DataLogger logger = !MainBuild ? new DataLogger(botName + "_" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss")) : null;

			if (Bot.MainBuild)
			{
				// Join a standard race
				writer.WriteLine(new Join(botName, botKey).ToJson());
			}
			else
			{
				// Create and join a single car race
				string trackName = 
					"keimola"
					//"france"
					//"germany"
					//"usa"
					;
				writer.WriteLine(new JoinRaceSolo(botName, botKey, trackName, "yellow").ToJson());

				// Create and join a multi car race
				//writer.WriteLine(new JoinRace(botName, botKey, trackName, "password", 2).ToJson());
			}

			// Pump messages
			string line;
			while ((line = reader.ReadLine()) != null)
			{
				if (logger != null)
                    logger.OnReceiveMessage(line);

				IncomingMsgWrapper rcvMsg = JsonConvert.DeserializeObject<IncomingMsgWrapper>(line);
      				
                SendMsg sendMsg = ai.OnReceiveMessage(rcvMsg);
				if (sendMsg != null)
					writer.WriteLine(sendMsg.ToJson());
			}

            if (logger != null)
                logger.Close();
		}

		System.Threading.Thread.Sleep(5000);
	}
}
