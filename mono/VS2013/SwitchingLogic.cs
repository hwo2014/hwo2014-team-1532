﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


class SwitchingLogic
{
	// Starting from this position & lane, what is our lane going to be in toIndex
	public static int TargetLane(Track track, int fromPieceIndex, int fromLane, int toIndex)
	{
		int lane = fromLane;
		int pieceIndex = fromPieceIndex;

		while (pieceIndex != toIndex)
		{
			Piece piece = track.Pieces[pieceIndex];
			if (piece.switcher)
				lane += SwitchLogic(track, pieceIndex, lane);
			pieceIndex = track.GetNextPieceIndex(pieceIndex);
		}

		return lane;
	}

	// Do we want to switch? return -1 = left, 0 = no switch, 1 = right
	public static int SwitchLogic(Track track, int switchIndex, int fromLane)
	{
		int nextSwitchIndex = track.GetNextPossibleSwitchIndex(switchIndex);
		int nextPiece = track.GetNextPieceIndex(switchIndex);

		double distanceLeft = fromLane > 0 ? track.GetDistanceBetween(nextPiece, 0.0, fromLane - 1, nextSwitchIndex, 0.0, fromLane - 1) : double.MaxValue;
		double distanceStraight = track.GetDistanceBetween(nextPiece, 0.0, fromLane, nextSwitchIndex, 0.0, fromLane);
		double distanceRight = fromLane + 1 < track.LaneCount ? track.GetDistanceBetween(nextPiece, 0.0, fromLane + 1, nextSwitchIndex, 0.0, fromLane + 1) : double.MaxValue;

		if (distanceLeft < Math.Min(distanceStraight, distanceRight))
			return -1;
		else if (distanceRight < Math.Min(distanceStraight, distanceLeft))
			return 1;

		return 0;
	}

	public static SendMsg SwitchingLogicToMsg(int decision)
	{
		if (decision == -1)
			return new SwitchLane(SwitchLane.Direction.Left);
		if (decision == 1)
			return new SwitchLane(SwitchLane.Direction.Right);

		return null;
	}
}

