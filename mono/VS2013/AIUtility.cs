﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


class AIUtility
{
	public static double Rad2Deg(double radians)
	{
		return radians * 180 / Math.PI;
	}

	public static double Deg2Rad(double degrees)
	{
		return degrees / 180 * Math.PI;
	}

    /// <summary>
    /// Given fromSpeed, engineForce and friction, what is the max speed we should reach before setting throttle to 0 to arrive at targetSpeed in targetDistance
    /// </summary>
    public static double GetMaxSpeed(double fromSpeed, Engine engine, double targetDistance, double targetSpeed)
    {
        double distance = targetDistance;
        double speed = fromSpeed;

        if (GetBrakingDistance(speed, targetSpeed, engine) > distance) // We are already fucked. Target speed is impossible.
        {
            return targetSpeed; // So we return our target speed
        }

		Engine clone = engine.GetClone();
		while (GetBrakingDistance(speed, targetSpeed, clone) < distance + GetSpeedAfterOneTick(speed, 1.0, clone)) // Is braking point still ahead? 
        {
            speed = GetSpeedAfterOneTick(speed, 1.0, clone);
            distance -= speed;
			clone.OnEndTick(0, 0.0);
        }

        return speed;
    }

    /// <summary>
    /// Given fromSpeed, engineForce and friction, what is the last possible braking point returned as distance from targetdistance
    /// </summary>
    public static double GetBrakingPoint(double fromSpeed, Engine engine, double targetDistance, double targetSpeed)
    {
        double distance = targetDistance;
        double speed = fromSpeed;

        if (GetBrakingDistance(speed, targetSpeed, engine) > distance)
        {
			return GetBrakingDistance(speed, targetSpeed, engine);
        }

		while (GetBrakingDistance(speed, targetSpeed, engine) < distance)
        {
			speed = GetSpeedAfterOneTick(speed, 1.0, engine);
            distance -= speed;
        }

        return distance;
    }

    public static double GetBrakingDistance(double fromSpeed, double toSpeed, Engine engine)
    {
        double distance = 0.0f;

        while (fromSpeed > toSpeed)
        {
			var a = -(fromSpeed * engine.Friction);
            var speedAfterTick = fromSpeed + a;
            distance += fromSpeed + a / 2;

            fromSpeed = speedAfterTick;
        }

        return distance;
    }

	public static double GetSpeedAfterOneTick(double fromSpeed, double throttle, Engine engine)
    {
        return (fromSpeed + engine.EnginePower * throttle - fromSpeed * engine.Friction);
    }

    public static double GetSpinDistance(double fromAngle, double angleSpeed, double fromSpeed, double throttle, double spinAngle, Engine engine)
    {
        double angle = fromAngle;
        double speed = fromSpeed;
        double distance = 0.0;

        if (angleSpeed == 0)
            return 0.0;

        while (Math.Abs(angle) < spinAngle)
        {
            speed = GetSpeedAfterOneTick(speed, throttle, engine);
            distance += speed;
            angle += angleSpeed;
        }

        return distance;
    }
}

